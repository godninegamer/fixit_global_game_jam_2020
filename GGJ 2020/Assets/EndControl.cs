﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndControl : MonoBehaviour
{
    public GameObject[] SceneEnding;

    public Button backMainMenu;
    // Start is called before the first frame update
    void Start()
    {
        for(int i =0; i < SceneEnding.Length; i++)
        {
            SceneEnding[i].SetActive(false);
        }


        
        backMainMenu.onClick.AddListener(delegate { backMainMenuClick(backMainMenu); });
    }

    // Update is called once per frame
    void Update()
    {
        switch (ScoreManager.Instance.LevelgetChoice[7])
        {
            case 1: SceneEnding[0].SetActive(true);
                break;
            case 2:
                SceneEnding[1].SetActive(true);
                break;
            case 3:
                SceneEnding[2].SetActive(true);
                break;
        }

            
    }
    

    public void backMainMenuClick(Button button)
    {
        SceneManager.LoadScene("MainMenu");
    }
}
