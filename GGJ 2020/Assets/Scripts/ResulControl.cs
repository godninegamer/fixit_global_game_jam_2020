﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class ResulControl : MonoBehaviour
{
    public Text[] Text;
    public int StartResult;
    //Levels in Result
    public int firstLevel;
    public int secondLevel;
    public int ThirdLevel;

    //First
    public GameObject[] FirstObjectForm;
    public GameObject[] SecondObjectForm;
    public GameObject[] ThirdObjectForm;

    public string _nameLevel;
    public Button nextLevel;
    public Button backMainMenu;

    private float playtime = 0;


    [SerializeField]
    public AudioSource _audiosouce;
    [SerializeField]
    public AudioClip ShowSound;

    //special Object
    public GameObject stage4anim;

    // Start is called before the first frame update
    void Start()
    {
        Text[0].text = "Level " + StartResult;
        Text[1].text = "Level " + (StartResult+1);
        Text[2].text = "Level " + (StartResult+2);

        _audiosouce = this.GetComponent<AudioSource>();
        _audiosouce.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[0];

        nextLevel.onClick.AddListener(delegate { nextLevelClick(nextLevel); });
        backMainMenu.onClick.AddListener(delegate { backMainMenuClick(backMainMenu); });

        for (int i = 0; i<FirstObjectForm.Length; i++)
        {
            FirstObjectForm[i].SetActive(false);
        }

        for (int i = 0; i < SecondObjectForm.Length; i++)
        {
            SecondObjectForm[i].SetActive(false);
        }

        for (int i = 0; i < ThirdObjectForm.Length; i++)
        {
            ThirdObjectForm[i].SetActive(false);
        }

        if(stage4anim!=null)
        stage4anim.SetActive(false);

        if (SoundManager.Instance.BGMSource.isPlaying)
            SoundManager.Instance.BGMSource.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        if (playtime < 15)
            playtime += Time.deltaTime;

        if (playtime >= 1)
        {
            CheckFirst(ScoreManager.Instance.LevelgetChoice[firstLevel]);
            

            if(FirstObjectForm[0].activeSelf==true&&stage4anim != null)
            {
                stage4anim.SetActive(true);
            }
        }
        if (playtime >= 3)
        {
            CheckSecond(ScoreManager.Instance.LevelgetChoice[secondLevel]);
            
        }
        if (playtime >= 5)
        {
            CheckThird(ScoreManager.Instance.LevelgetChoice[ThirdLevel]);
            
        }

    }

    public void nextLevelClick (Button button)
    {
        SceneManager.LoadScene(_nameLevel);
    }

    public void backMainMenuClick (Button button)
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void CheckFirst(int getChoice)
    {
        switch (getChoice)
        {
            case 1: FirstObjectForm[0].SetActive(true);
                break;
            case 2: FirstObjectForm[1].SetActive(true);
                break;
            case 3: FirstObjectForm[2].SetActive(true);

                break;
        }
    }

    public void CheckSecond(int getChoice)
    {
        switch (getChoice)
        {
            case 1:
                SecondObjectForm[0].SetActive(true);
                
                break;
            case 2:
                SecondObjectForm[1].SetActive(true);
                
                break;
            case 3:
                SecondObjectForm[2].SetActive(true);
                
                break;
        }
    }

    public void CheckThird(int getChoice)
    {
        switch (getChoice)
        {
            case 1:
                ThirdObjectForm[0].SetActive(true);
                if (_audiosouce.isPlaying)
                    _audiosouce.Stop();

                _audiosouce.PlayOneShot(ShowSound);
                break;
            case 2:
                ThirdObjectForm[1].SetActive(true);
                if (_audiosouce.isPlaying)
                    _audiosouce.Stop();

                _audiosouce.PlayOneShot(ShowSound);
                break;
            case 3:
                ThirdObjectForm[2].SetActive(true);
                if (_audiosouce.isPlaying)
                    _audiosouce.Stop();

                _audiosouce.PlayOneShot(ShowSound);
                break;
        }
    }
}
