﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class StageControl : MonoBehaviour
{
    public string _nameScene;

    public int currentLevel;

    public static bool GameIsPaused = false;

    public GameObject pauseUIMenu;

    [SerializeField] Button _Choice1;
    [SerializeField] Button _Choice2;
    [SerializeField] Button _Choice3;

    [SerializeField] Button _Pause;
    [SerializeField] Button _Resume;
    [SerializeField] Button _backMainMenu;

    [Range(0,1)]
    public int _CheckChoice1;
    [Range(0, 1)]
    public int _CheckChoice2;
    [Range(0, 1)]
    public int _CheckChoice3;

    [SerializeField]
    public AudioSource _audiosouce;
    [SerializeField]
    public AudioClip _Clicksound;

    private float timedelay;
    private bool CheckClick = false;

    void Start()
    {
        _Choice1.onClick.AddListener(delegate { ClickButton1(_Choice1); });
        _Choice2.onClick.AddListener(delegate { ClickButton2(_Choice2); });
        _Choice3.onClick.AddListener(delegate { ClickButton3(_Choice3); });

        _Pause.onClick.AddListener(delegate { PauseClick(_Pause); });
        _Resume.onClick.AddListener(delegate { ResumeClick(_Resume); });
        _backMainMenu.onClick.AddListener(delegate { backMainMenuClick(_backMainMenu); });

        _audiosouce = this.GetComponent<AudioSource>();
        _audiosouce.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        if (!SoundManager.Instance.BGMSource.isPlaying)
            SoundManager.Instance.BGMSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                MenuPause();
            }
        }

        if (CheckClick == true)
        {
            timedelay += Time.deltaTime;
            if(timedelay >= 1)
            {
                SceneManager.LoadScene(_nameScene);
            }
        }
    }


    //Event,When click
    public void ClickButton1(Button button)
    {
        ScoreManager.Instance.AddScore(_CheckChoice1);
        ScoreManager.Instance.LevelgetChoice[currentLevel] = 1;
        CheckClick = true;

        if (_audiosouce.isPlaying)
            _audiosouce.Stop();

        _audiosouce.PlayOneShot(_Clicksound);
    }

    public void ClickButton2(Button button)
    {
        ScoreManager.Instance.AddScore(_CheckChoice2);
        ScoreManager.Instance.LevelgetChoice[currentLevel] = 2;
        CheckClick = true;

        if (_audiosouce.isPlaying)
            _audiosouce.Stop();

        _audiosouce.PlayOneShot(_Clicksound);
    }

    public void ClickButton3(Button button)
    {
        ScoreManager.Instance.AddScore(_CheckChoice3);
        ScoreManager.Instance.LevelgetChoice[currentLevel] = 3;
        CheckClick = true;

        if (_audiosouce.isPlaying)
            _audiosouce.Stop();

        _audiosouce.PlayOneShot(_Clicksound);
    }

    //Pause Menu
    public void PauseClick(Button button)
    {
        MenuPause();
    }

    public void ResumeClick (Button button)
    {
        Resume();
    }

    public void backMainMenuClick (Button button)
    {
        SceneManager.LoadScene("MainMenu");
        
    }

    public void Resume()
    {
        pauseUIMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void MenuPause()
    {
        pauseUIMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

}
