﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;

public class SceneResult1 : MonoBehaviour
{
    
    public GameObject Bike;

    public GameObject Dio;

    public GameObject CavasImage;
    public GameObject CavasLevel;

    public GameObject _keepSoundJojo;
    public GameObject _keepSoundDio;

    [SerializeField]
    public AudioSource tobeContinu;
    [SerializeField]
    public AudioClip SoundJojo;

    [SerializeField]
    public AudioSource AudioDio;
    [SerializeField]
    public AudioClip voiceDio;


    public float timedelay=0;

    void Start()
    {
        Dio.SetActive(false);

        tobeContinu = _keepSoundJojo.GetComponent<AudioSource>();
        tobeContinu.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[0];

        AudioDio = _keepSoundDio.GetComponent<AudioSource>();
        AudioDio.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("Voice")[0];

        _keepSoundJojo.SetActive(false);
        _keepSoundDio.SetActive(false);


    }

    
    void Update()
    {
        if(Bike.activeSelf == true)
        {
            _keepSoundJojo.SetActive(true);
            timedelay += Time.deltaTime;
            if(timedelay >= 1.5)
            {
                _keepSoundDio.SetActive(true);
            }
            if(timedelay >= 4)
            {
                Dio.SetActive(true);
            }
            if(timedelay >= 4.25)
            {
                CavasImage.SetActive(false);
                CavasLevel.SetActive(false);
            }

        }
    }
}
