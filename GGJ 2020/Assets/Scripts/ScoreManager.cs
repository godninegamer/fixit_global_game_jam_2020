﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get { return _instance; } }
    private static ScoreManager _instance;

    public int countLevel;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void Start()
    {
        
    }

    public int Score
    {
        get { return _Score; }
        set { _Score = value; }

    }

    int _Score;

    public int[] LevelgetChoice
    {
        get { return _LevelgetChoice; }
        set { _LevelgetChoice = value; }
    }

    int[] _LevelgetChoice = new int[10];

    public int LevelChoice1
    {
        get { return _LevelChoice1; }
        set { _LevelChoice1 = value; }
    }

    int _LevelChoice1;
    

    public void AddScore (int add)
    {
        _Score += add;
        
    }

    public void ResetScore()
    {
        _Score = 0;
    }

    private void Update()
    {
        //Debug.Log(_LevelgetChoice[0]);
    }

}
