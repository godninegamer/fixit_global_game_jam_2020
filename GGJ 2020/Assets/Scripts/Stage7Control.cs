﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Stage7Control : MonoBehaviour
{
    public string _nameLevel;

    public GameObject AnimInFirst;
    public GameObject AnimInSecond;
    public GameObject AnimInThird;

    [SerializeField]
    public Button NextStage;
    [SerializeField]
    public Button BackMainMenu;

    // Start is called before the first frame update
    void Start()
    {
        AnimInFirst.SetActive(false);
        AnimInSecond.SetActive(false);
        AnimInThird.SetActive(false);

        NextStage.onClick.AddListener(delegate { nextStageClick(NextStage); });
        BackMainMenu.onClick.AddListener(delegate { backMainMenuClick(BackMainMenu); });
    }

    // Update is called once per frame
    void Update()
    {
        switch (ScoreManager.Instance.LevelgetChoice[6])
        {
            case 1:
                AnimInFirst.SetActive(true);
                break;
            case 2:
                AnimInSecond.SetActive(true);
                break;
            case 3:
                AnimInThird.SetActive(true);
                break;
        }
            
    }

    public void nextStageClick(Button button)
    {
        SceneManager.LoadScene(_nameLevel);
    }

    public void backMainMenuClick(Button button)
    {
        SceneManager.LoadScene("MainMenu");
    }
}
