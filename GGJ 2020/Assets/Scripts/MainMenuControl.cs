﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour
{
    [SerializeField] Button _PlayButton;
    [SerializeField] Button _ExitButton;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        _PlayButton.onClick.AddListener(delegate { PlayButton(_PlayButton); });
        _ExitButton.onClick.AddListener(delegate { ExitButton(_ExitButton); });

        if (!SoundManager.Instance.BGMSource.isPlaying)
            SoundManager.Instance.BGMSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void PlayButton(Button button)
    {
        SceneManager.LoadScene("Stage1");
    }


    public void ExitButton(Button button)
    {
        Application.Quit();
    }
}
